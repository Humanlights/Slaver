﻿using Humanlights;
using Slaver.Services;
using System.Threading.Tasks;

namespace Slaver.Commands
{
    [Factory]
    public class Runtime : CommandLibrary
    {
        [Command ( Subname = "start", Help = "Starts a job execution" )]
        public static void StartJob ()
        {
            Task.Run ( async delegate { await Executor.ExecuteJob ( Parameter ); } );
        }

        [Command ( Subname = "stop", Help = "Stops a current job that's being executed" )]
        public static void StopJob ()
        {
            Task.Run ( async delegate { await Executor.StopJob ( Parameter ); } );
        }

        [Command ( Subname = "timeout", Help = "Timeout in between stage executions." )]
        public static int TimeoutMiliseconds = 500;
    }
}
