﻿using Humanlights;
using Humanlights.Extensions;
using Slaver.Components;
using Slaver.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Slaver.Commands
{
    [Factory]
    public class Code : CommandLibrary
    {
        public static void ExecuteBat ( string line )
        {
            Task.Run ( async delegate { Executor.GetRuntimeJob ( RuntimeConfigs.Job.ID ).Process = await Executor.ExecuteBat ( line ); } ).Wait ();
        }

        public static void Log ( string text )
        {
            Write ( $"     [{text}]" );
        }

        [Command ( Subname = "cleanup" )]
        public static void Cleanup ()
        {
            OsEx.Folder.DeleteContents ( RuntimeConfigs.Job.WorkspaceFolder );
        }

        [Command ( Subname = "skip" )]
        public static void Skip () { }

        [Command ( Subname = "log" )]
        public static void PrintLog ()
        {
            Log ( RuntimeConfigs.Value.TrimStart ( '\"' ).TrimEnd ( '\"' ) );
        }

        [Command ( Subname = "bat" )]
        public static void Bat ()
        {
            // Write ( RuntimeConfigs.Value );
            ExecuteBat ( RuntimeConfigs.Value );
        }

        [Command ( Subname = "unity" )]
        public static void SetUnityPath ()
        {
            RuntimeConfigs.UnityPath = RuntimeConfigs.Value;
            Log ( $"Changed UnityEditor Path" );
        }

        [Command ( Subname = "setuser" )]
        public static void SetUsername ()
        {
            RuntimeConfigs.Username = RuntimeConfigs.Value;
            ExecuteBat ( $"git config user.name {RuntimeConfigs.Username}" );
            Log ( "Changed global Git Username" );
        }

        [Command ( Subname = "setpass" )]
        public static void SetPassword ()
        {
            RuntimeConfigs.Password = RuntimeConfigs.Value;
            ExecuteBat ( $"git config user.password {RuntimeConfigs.Password}" );
            Log ( "Changed global Git Password" );
        }

        [Command ( Subname = "checkout" )]
        public static void Checkout ()
        {
            Log ( $"Checking out {RuntimeConfigs.Value}" );
            ExecuteBat ( $"git clone {RuntimeConfigs.Value} {RuntimeConfigs.Job.WorkspaceFolder}" );
            Log ( $"Done!" );
        }

        [Command ( Subname = "clearbuild" )]
        public static void ClearBuild ()
        {
            Log ( "Clearing the build" );
            OsEx.Folder.DeleteContents ( RuntimeConfigs.Job.BuildFolder );
        }

        [Command ( Subname = "param" )]
        public static void AddParameter ()
        {
            var split = RuntimeConfigs.Value.Split ( ' ' );
            var parameterName = split [ 0 ];
            var parameterValue = split.ToString ( 1 ).TrimStart ( '\"' ).TrimEnd ( '\"' );

            RuntimeConfigs.Parameters.Add ( parameterName, RuntimeConfigs.Replacer ( parameterValue ) );
            Log ( $"{parameterName}: {RuntimeConfigs.Replacer ( parameterValue )}" );
        }

        [Command ( Subname = "artifact" )]
        public static void Artifact ()
        {
            var directoryPath = RuntimeConfigs.Value.Replace ( "\"", "" );
            directoryPath = System.IO.Path.GetFullPath ( directoryPath );

            var directorySplits = directoryPath.Split ( '\\' );
            var directoryName = directorySplits [ directorySplits.Length - 1 ];
            var fileName = $"{RuntimeConfigs.Job.ID}_{RuntimeConfigs.Job.BuildNumber}.tar.gz";
            Log ( $"Archiving: {directoryName}.tar.gz" );

            OsEx.Folder.Create ( RuntimeConfigs.Job.ArtifactFolder );

            Application.ZipFolder ( directoryPath, $"{RuntimeConfigs.Job.ArtifactFolder}\\{fileName}" );
        }
    }

    public class RuntimeConfigs
    {
        private static string _value { get; set; }
        public static string Value
        {
            get { return _value; }
            set
            {
                try { _value = Replacer ( value ); } catch { _value = value; }
            }
        }
        public static Job Job { get; set; }

        public static string Username { get; set; }
        public static string Password { get; set; }

        public static string UnityPath { get; set; }
        public static Dictionary<string, string> Parameters { get; set; } = new Dictionary<string, string> ();

        public static string Replacer ( string text, bool accessParameters = true )
        {
            text = text
                .Replace ( "[workspace]", $"{Job.WorkspaceFolder}" ).Replace ( "[WORKSPACE]", $"{Job.WorkspaceFolder}" )
                .Replace ( "[unity]", $"{UnityPath}" ).Replace ( "[UNITY]", $"{UnityPath}" )
                .Replace ( "[build]", $"{Job.BuildFolder}" ).Replace ( "[BUILD]", $"{Job.BuildFolder}" );

            foreach ( var param in Parameters )
            {
                if ( accessParameters ) text = text.Replace ( $"[{param.Key}]", param.Value );
            }

            return text;
        }
    }
}
