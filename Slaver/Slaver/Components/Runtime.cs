﻿using System.Diagnostics;

namespace Slaver.Components
{
    public class Runtime
    {
        public string JobId { get; set; }
        public Process Process { get; set; }

        public Runtime () { }
        public Runtime ( string jobId )
        {
            JobId = jobId;
        }
        public Runtime ( string jobId, Process process )
        {
            JobId = jobId;
            Process = process;
        }
    }
}
