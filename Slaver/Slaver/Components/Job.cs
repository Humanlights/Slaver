﻿namespace Slaver.Components
{
    [System.Serializable]
    public class Job
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string WorkspaceFolder { get; set; }
        public string BuildFolder { get; set; }
        public int BuildNumber { get; set; }
        public string ArtifactFolder { get; set; }
        public Stage [] Stages { get; set; }
        public bool SkipErrors { get; set; } = false;
    }
}