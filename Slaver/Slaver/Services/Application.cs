﻿using Humanlights.Extensions;
using Slaver.Components;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Slaver.Services
{
    public class Application
    {
        public static string RootFolder { get { return AppDomain.CurrentDomain.BaseDirectory; } }
        public static string JobFolder { get { var folder = $"{RootFolder}Jobs"; OsEx.Folder.Create ( folder ); return folder; } }
        public static string WorkspacesFolder { get { var folder = $"{RootFolder}Workspaces"; OsEx.Folder.Create ( folder ); return folder; } }
        public static string BuildsFolder { get { var folder = $"{RootFolder}Builds"; OsEx.Folder.Create ( folder ); return folder; } }
        public static string ArtifactsFolder { get { var folder = $"{RootFolder}Artifacts"; OsEx.Folder.Create ( folder ); return folder; } }

        public static string TempFolder { get { var folder = $"{RootFolder}Temp"; OsEx.Folder.Create ( folder ); return folder; } }
        public static string [] JobFiles { get { return OsEx.Folder.GetFilesWithExtension ( JobFolder, "sjob", System.IO.SearchOption.TopDirectoryOnly ); } }

        public static void Cleanup ()
        {
            OsEx.Folder.Create ( TempFolder, true );
        }

        public static string GetValue ( string [] lines, string parameter )
        {
            var line = lines.FirstOrDefault ( x => x.ToLower ().StartsWith ( parameter ) );
            if ( string.IsNullOrEmpty ( line ) ) return string.Empty;

            return line.Replace ( parameter, "" ).Replace ( "\"", "" ).Trim ();
        }
        public static string GetValue ( string line, string parameter )
        {
            if ( string.IsNullOrEmpty ( line ) ) return string.Empty;

            return line.Replace ( parameter, "" ).Replace ( "\"", "" ).Trim ();
        }
        public static Stage [] GetStages ( string [] lines )
        {
            var stages = new List<Stage> ();

            Stage currentStage = null;
            foreach ( var line in lines )
            {
                if ( string.IsNullOrEmpty ( line ) ) continue;

                if ( currentStage == null )
                {
                    if ( line.StartsWith ( "startstage" ) )
                    {
                        // Console.WriteLine ( $"Start: {line}" );
                        currentStage = new Stage ();
                        currentStage.Name = GetValue ( line, "startstage" );
                    }
                }
                else
                {
                    if ( line.StartsWith ( "endstage" ) )
                    {
                        // Console.WriteLine ( $"End: {line}" );
                        stages.Add ( currentStage.MakeCopy () );
                        currentStage = null;
                    }
                    else
                    {
                        // Console.WriteLine ( $"Process: {line}" );
                        currentStage.Lines.Add ( line.TrimStart ( '\"' ).TrimEnd ( '\"' ) );
                    }
                }
            }

            // foreach ( var stage in stages )
            // {
            //     Console.WriteLine ( $"Final: {stage.Name}, {stage.Lines.Count}" );
            // }

            return stages.ToArray ();
        }

        public static int GetBuildNumber ( string jobId )
        {
            var file = $"{JobFolder}\\{jobId}.buildnr";

            if ( !OsEx.File.Exists ( file ) ) OsEx.File.Create ( file, "1" );

            return StringEx.ToInt ( OsEx.File.ReadText ( file ) );
        }

        public static int AppendBuildNumber ( string jobId )
        {
            var file = $"{JobFolder}\\{jobId}.buildnr";

            if ( !OsEx.File.Exists ( file ) ) OsEx.File.Create ( file, "1" );
            else
            {
                var value = StringEx.ToInt ( OsEx.File.ReadText ( file ) );
                OsEx.File.Create ( file, $"{value + 1}" );

                return value;
            }

            return 1;
        }

        public static void ZipFolder ( string folderPath, string compressedFile )
        {
            System.IO.Compression.ZipFile.CreateFromDirectory ( folderPath, compressedFile );
        }
    }
}
